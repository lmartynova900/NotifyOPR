// ==UserScript==
// @name         NotifyOPR
// @namespace    http://tampermonkey.net/
// @version      5.3
// @description  Lets make some PoIs!
// @author       HPWU-PoGO-Community
// @match        https://wayfarer.nianticlabs.com/review
// @grant        unsafeWindow
// @grant        GM_notification
// @grant        GM_addStyle
// @grant        GM_setValue
// @grant        GM_getValue
// @downloadURL  https://gitlab.com/ruslan.levitskiy/NotifyOPR/raw/master/NotifyOPR.user.js
// @require 	 https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// @require 	 https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js
// @require 	 https://rsccdn.ru/static/notifyopr.min.js?202010300001

// ==/UserScript==
var reloadtime=3;
var soundURL='https://www.soundjay.com/button/button-37.mp3';
var page;
var controller;
var expireTime;
var timer;
var notificationDetails;
var userLang = navigator.language || navigator.userLanguage || 'en';

var exifObj = {};

var exifFields = [
    'DateTime',
    // 'ImageWidth',
    // 'ImageHeight',
    'Make',
    'Model',
    'Software'
];

var exifMarker;
var exifMarker2;
var exifMarkerLocationChange;
var exifPosition;

const NOTIF_ON = "Notifications enabled | Sound enabled";
const NOTIF_OFF = "Notifications disabled | Sound muted";
const NOTIF_SOUND_OFF = "Notifications enabled | Sound muted";

const COMPAT_ON = "Compatibility mode enabled";
const COMPAT_OFF = "Compatibility mode disabled";

function getRandomInt(min,max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function timeFunct() {
    'use strict';
    location.reload();
}

function check(){
    if (angular.element(document.getElementById("ReviewController")).scope() === undefined) {
        console.log('Not loaded, wait more time.');
        window.setTimeout(check, 1000);
    } else {
        var errorMessage = angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.errorMessages;
        page = angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.pageData;
        console.log('Checking sumbision');
        if (errorMessage.length == 0 && page === undefined) {
            console.log('Not loaded, wait more time.');
            window.setTimeout(check, 1000);
        } else if (page !== undefined) {
            console.log('Find portal ');
            var audio = new Audio(soundURL);
            if (GM_getValue('isSoundOff') == 0) {
                audio.play();
            }
            notificationDetails = {
                title: page.title,
                text: 'Адрес: ' + page.streetAddress,
                timeout: 100000,
                highlight: true,
                image: page.imageUrl,
                onclick: function () {
                    console.log("Notice clicked.");
                    window.focus();
                }
            };

            notify('');

            if (angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.reviewType == "NEW") {
                controller = angular.element(document.getElementById("review-new-component")).scope().$ctrl;
            } else {
                controller = angular.element(document.querySelector(".edit-container")).scope().$ctrl;
            }

            setTimeout(function () {
                pageTweaks();
                if (angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.reviewType != "NEW") {
                    var intelBtn = $('<a class="button-primary" id="intelLinkEdit" target="intel" href="https://www.ingress.com/intel?ll=' + page.lat + ',' + page.lng + '&z=17&pll=' + page.lat + ',' + page.lng + '">Intel</a>');
                    $("#location-edit-map").before(intelBtn);

                    var actualPosition = {lat: page.lat, lng: page.lng};
                    var markerActualLocation = new google.maps.Marker({
                        position: actualPosition,
                        title: 'Actual Position',
                        zIndex: -10000
                    });
                    markerActualLocation.setMap(controller.locationEditsMap);
                }


                expireTime = new Date(page.expires);
                $('body').append('<h4 id="timer">');
                timer = setInterval(function () {
                    var timerInfo = getTimer();
                    $('#timer').text(timerInfo.timerStr);
                    if (timerInfo.isExpired) {
                        $('#timer').addClass('expired');
                    }
                }, 1000);
                $("body").append('<span id="helpIcon" class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-trigger="click hover" data-html="true" data-title="Keys <kbd>1</kbd>-<kbd>5</kbd> to vote,<br><kbd>D</kbd> to mark as duplicate,<br><kbd>space</kbd>/<kbd>enter</kbd> to confirm dialogs,<br><kbd>esc</kbd> or numpad <kbd>/</kbd> to reset selector,<br>numpad <kbd>+</kbd> <kbd>-</kbd> to navigate" data-placement="bottom"></span>');
                $('[data-toggle="tooltip"]').tooltip();
                if (GM_getValue('isSoundOff') != -1) {
                    GM_notification(notificationDetails);
                    var reminder = function () {
                        var timerInfo = getTimer();
                        if (!timerInfo.isExpired) {
                            var audio = new Audio(soundURL);
                            if (!GM_getValue('isSoundOff')) {
                                audio.play();
                            }
                            GM_notification(notificationDetails);
                            setTimeout(reminder, 5 * 60 * 1000);
                        }
                    };
                    setTimeout(reminder, 5 * 60 * 1000);
                }
            }, 500);
        } else {
            console.log('Wait time to reload.');
            var a = getRandomInt(1000 * 60 * (reloadtime - 1), 1000 * 60 * reloadtime);
            window.setTimeout(timeFunct, a);
        }
    }
}

function initCtrls(){
    console.log("init interface");

    $("body").on("exifLoaded", function(e, exifObj) {
        $('#exifDiv').remove();
        var GPSLat;
        var GPSLng;
        var GPSDelta;
        var exifStr = '';
        var exifDiv = $('<div id="exifDiv" style="clear: both;">');
        if (exifObj) {
            if (exifObj.GPSLatitude != undefined && exifObj.GPSLongitude != undefined) {
                var latHem = 1;
                if (exifObj.GPSLatitudeRef == 'S') {
                    latHem = -1;
                }
                var lngHem = 1;
                if (exifObj.GPSLongitudeRef == 'W') {
                    lngHem = -1;
                }
                GPSLat = (parseFloat(exifObj.GPSLatitude[0]) + parseFloat(exifObj.GPSLatitude[1] / 60) + parseFloat(exifObj.GPSLatitude[2] / 3600)) * latHem;
                GPSLng = (parseFloat(exifObj.GPSLongitude[0]) + parseFloat(exifObj.GPSLongitude[1] / 60) + parseFloat(exifObj.GPSLongitude[2] / 3600)) * lngHem;
                GPSDelta = Math.round10(latlng2distance(page.lat, page.lng, GPSLat, GPSLng), -2);
            }
            if (GPSLat && GPSLng) {
                var deltaColor;
                if (GPSDelta > 60) {
                    deltaColor = 'danger';
                } else if (GPSDelta > 20) {
                    deltaColor = 'warning';
                } else {
                    deltaColor = 'success';
                }
                if (angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.reviewType == "NEW") {
                    exifStr += '<span class="label label-'+deltaColor+'"><a href="http://maps.google.com/?q=' + GPSLat + ',' + GPSLng + '" title="' + GPSLat + ' / ' + GPSLng + '" target="_blank" style="font-weight: bold;">GPS Δ' + GPSDelta + 'm</a></span> ';
                } else {
                    exifStr += '<span class="label label-primary"><a href="https://www.ingress.com/intel?z=17&ll=' + GPSLat + ',' + GPSLng + '" title="' + GPSLat + ' / ' + GPSLng + '" target="_blank" style="font-weight: bold;" class="bg-primary">GPS</a></span> ';
                }
                exifPosition = {lat: GPSLat, lng: GPSLng};
                exifMarker = new google.maps.Marker({
                    position: exifPosition,
                    title: 'EXIF Position',
                    icon: EXIF_MAP_MARKER
                });
                exifMarker2 = new google.maps.Marker({
                    position: exifPosition,
                    title: 'EXIF Position',
                    icon: EXIF_MAP_MARKER
                });
                exifMarkerLocationChange = new google.maps.Marker({
                    position: exifPosition,
                    title: 'EXIF Position',
                    icon: EXIF_MAP_MARKER,
                    zIndex: -10000
                });
                exifMarker.setMap(controller.map2);
                exifMarker2.setMap(controller.map);
                exifMarkerLocationChange.setMap(controller.locationEditsMap);
                $(document).on('click', 'span.clickable', function () {
                    exifMarker.setMap(controller.map);
                    exifMarker2.setMap(controller.map2);
                    exifMarkerLocationChange.setMap(controller.locationEditsMap);
                });
            }
            $.each(exifFields, function (index, value) {
                if (exifObj[value] != undefined) {
                    var val = exifObj[value].trim();
                    exifStr += "<span class='label label-primary'>"+val+"</span> ";
                }
            });

            if (!exifStr) {
                exifStr = ' EMPTY';
            }
        } else {
            exifStr = ' NOT FOUND';
        }
        exifDiv.append(exifStr);
        if (angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.reviewType == "NEW") {
            if (controller.hasSupportingImageOrStatement) {
                $("portalphoto").after(exifDiv);
            } else {
                exifDiv.appendTo('#descriptionDiv');
            }
        } else {
            $("#location-edit-map").before(exifDiv);
        }
    });

    $('body').append('<button class="sndBtn" style="margin:0; width:49px; height: 45px;" title="Notifications"><span id="sndBtnImg" class="glyphicon" style="font-size: 20px;"></span></button>');
    if (GM_getValue('isSoundOff') == 1) {
        $('#sndBtnImg').addClass('glyphicon-volume-off');
        $('.sndBtn').attr('title', NOTIF_SOUND_OFF);
    } else if (GM_getValue('isSoundOff') == -1) {
        $('#sndBtnImg').addClass('glyphicon-remove');
        $('.sndBtn').attr('title', NOTIF_OFF);
    } else {
        $('#sndBtnImg').addClass('glyphicon-volume-up');
        $('.sndBtn').attr('title', NOTIF_ON);
    }
    $('body').on('click', 'button.sndBtn', function (e) {
        if (GM_getValue('isSoundOff') == 1) {
            GM_setValue('isSoundOff', -1);
            $('#sndBtnImg').removeClass('glyphicon-volume-off');
            $('#sndBtnImg').addClass('glyphicon-remove');
            $('.sndBtn').attr('title', NOTIF_OFF);
        } else if (GM_getValue('isSoundOff') == -1) {
            GM_setValue('isSoundOff', 0);
            $('#sndBtnImg').removeClass('glyphicon-volume-off');
            $('#sndBtnImg').addClass('glyphicon-volume-up');
            $('.sndBtn').attr('title', NOTIF_ON);
        } else {
            GM_setValue('isSoundOff', 1);
            $('#sndBtnImg').removeClass('glyphicon-volume-up');
            $('#sndBtnImg').addClass('glyphicon-volume-off');
            $('.sndBtn').attr('title', NOTIF_OFF);
        }
    });

    $('body').on('click', '.reset-map-icon', function () {
        initMaps();
    })
}

function pageTweaks() {
    var rosreestr = degrees2meters(page.lat, page.lng);
    var buttonsDiv = $('<div id="mapBtns">');
    $('<a class="clickable" target="imageSearch" href="https://www.google.co.in/searchbyimage?image_url='+page.imageUrl+'&encoded_image=&image_content=&filename=&hl=ru" title="Image Search"><img src="https://rsccdn.ru/static/icons/is.png"></a>').appendTo(buttonsDiv);
    $('<a class="clickable" target="intel" href="https://www.ingress.com/intel?ll=' + page.lat + ',' + page.lng + '&z=17" title="Intel"><img src="https://rsccdn.ru/static/icons/iitc.png"></a>').appendTo(buttonsDiv);
    $('<a class="clickable" target="osm" href="https://www.openstreetmap.org/#map=17/' + page.lat + '/' + page.lng + '" title="OpenStreetMap"><img src="https://rsccdn.ru/static/icons/osm.png"></a>').appendTo(buttonsDiv);
    $('<a class="clickable" target="yandex" href="https://yandex.ru/maps/?ll=' + page.lng + ',' + page.lat + '&z=17&l=sat,skl" title="Yandex"><img src="https://rsccdn.ru/static/icons/ym.png"></a>').appendTo(buttonsDiv);
    $('<a class="clickable" target="wiki" href="http://wikimapia.org/#lang=ru&lat=' + page.lat + '&lon=' + page.lng + '&z=17&m=b" title="WikiMapia"><img src="https://rsccdn.ru/static/icons/wm.png"></a>').appendTo(buttonsDiv);
    $('<a class="clickable" target="rkk" href="https://pkk5.rosreestr.ru/#x=' + rosreestr[0] + '&y=' + rosreestr[1] + '&z=17"><img src="https://rsccdn.ru/static/icons/pkk.png"></a>').appendTo(buttonsDiv);
    $("div#descriptionDiv div.card__footer").append(buttonsDiv);

    $("h1.title-description").append('<a class="transTitle transBtn" target="_blank" href="https://translate.google.com/#auto/'+userLang+'/'+encodeURIComponent(page.title)+'" title="Translate"><img src="https://rsccdn.ru/static/icons/trans.png"></a>');
    $("h4.title-description").append('<a class="transDescr transBtn" target="_blank" href="https://translate.google.com/#auto/'+userLang+'/'+encodeURIComponent(page.description)+'" title="Translate"><img src="https://rsccdn.ru/static/icons/trans.png"></a>');

    initMaps();
    if (angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.reviewType == "NEW") {
        oprToolsTweaks();
    }
}

function drawGridForMap(map, mapType) {
    drawCellGrid(map);
    map.addListener('bounds_changed', function () {
        drawCellGrid(map);
    });
}

function removeGridForMap(map, mapType) {
    map.data.forEach(function (feature) {
        map.data.remove(feature);
    });
    google.maps.event.clearListeners(map, 'bounds_changed');
}

function initMaps() {
    var mapOptions = {
        scrollwheel: true,
        gestureHandling: "greedy"
    };

    if (angular.element(document.getElementById("ReviewController")).scope().reviewCtrl.reviewType == "NEW") {
        controller.map.setOptions(mapOptions);
        controller.map2.setOptions(mapOptions);

        if (GM_getValue('s2_'+'dups')) {
            drawGridForMap(controller.map, 'dups');
        }
        btnsControlDivDups = document.createElement('div');
        btnsControlDups = new ControlBtns(btnsControlDivDups, controller.map, 'dups');
        btnsControlDivDups.index = 1;
        controller.map.controls[google.maps.ControlPosition.LEFT_TOP].clear();
        controller.map.controls[google.maps.ControlPosition.LEFT_TOP].push(btnsControlDivDups);

        if (GM_getValue('s2_'+'loc')) {
            drawGridForMap(controller.map2, 'loc');
        }
        btnsControlDivLoc = document.createElement('div');
        btnsControlLoc = new ControlBtns(btnsControlDivLoc, controller.map2, 'loc');
        btnsControlDivLoc.index = 1;
        controller.map2.controls[google.maps.ControlPosition.LEFT_TOP].clear();
        controller.map2.controls[google.maps.ControlPosition.LEFT_TOP].push(btnsControlDivLoc);
    } else {
        controller.locationEditsMap.setOptions(mapOptions);
        if (GM_getValue('s2_'+'edit')) {
            drawGridForMap(controller.locationEditsMap, 'edit');
        }
        btnsControlDivEdit = document.createElement('div');
        btnsControlEdit = new ControlBtns(btnsControlDivEdit, controller.locationEditsMap, 'edit');
        btnsControlDivEdit.index = 1;
        controller.locationEditsMap.controls[google.maps.ControlPosition.LEFT_TOP].clear();
        controller.locationEditsMap.controls[google.maps.ControlPosition.LEFT_TOP].push(btnsControlDivEdit);
    }
}

/**
 * The code of this function was stolen from the project OPR Tools because I'm too lazy to re-write it.
 * Thanks dude. Here is a link to his project: https://gitlab.com/1110101/opr-tools/
 */
function oprToolsTweaks() {
    const w = typeof unsafeWindow == "undefined" ? window : unsafeWindow;

    const el = w.document.querySelector("[ng-app='portalApp']");
    w.$app = w.angular.element(el);
    w.$injector = w.$app.injector();
    w.$rootScope = w.$app.scope();

    w.$scope = element => w.angular.element(element).scope();
    const subMissionDiv = w.document.getElementById(("review-new-component"));
    const subController = angular.element(subMissionDiv).scope().$ctrl;
    const newPortalData = subController.pageData;

    const answerDiv = w.document.getElementById("ReviewController");
    const ansController = angular.element(w.document.getElementById("ReviewController")).scope().reviewCtrl;
    if (!detectMob()) {
        try {
            const e = w.document.querySelector("#map-filmstrip > ul > li:nth-child(1) > img");
            if (e !== null) {
                setTimeout(() => {
                    e.click();
                }, 500);
            }
        } catch (err) {
        }
    }
    const filmstrip = w.document.getElementById("map-filmstrip");
    let lastScrollLeft = filmstrip.scrollLeft;

    function detectMob() {
        return ( window.innerWidth <= 800 ) ;
    }

    function scrollHorizontally(e) {
        e = window.event || e;
        if (("deltaY" in e && e.deltaY !== 0 || "wheelDeltaY" in e && e.wheelDeltaY !== 0) && lastScrollLeft === filmstrip.scrollLeft) {
            e.preventDefault();
            const delta = (e.wheelDeltaY || -e.deltaY * 25 || -e.detail);
            filmstrip.scrollLeft -= (delta);
            lastScrollLeft = filmstrip.scrollLeft;
        }
    }

    filmstrip.addEventListener("wheel", exportFunction(scrollHorizontally, w), false);
    filmstrip.addEventListener("DOMMouseScroll", exportFunction(scrollHorizontally, w), false);
    let submitButtons = w.document.querySelectorAll(".submit-btn-container");
    let submitButton = submitButtons[1].querySelector("button.submit");

    // keyboard navigation
    // keys 1-5 to vote
    // space/enter to confirm dialogs
    // esc or numpad "/" to reset selector
    // Numpad + - to navigate

    let currentSelectable = 0;
    let maxItems = 7;

    // a list of all 6 star button rows, and the two submit buttons
    let starsAndSubmitButtons;
    starsAndSubmitButtons = w.document.querySelectorAll(".five-star-rating, .bottom-btns [ng-click='reviewCtrl.submitForm()']");

    function highlight() {
        starsAndSubmitButtons.forEach(exportFunction((element) => { element.style.border = "none"; }, w));
        starsAndSubmitButtons.forEach(exportFunction((element) => { element.style.padding = "1px"; }, w));
        if (currentSelectable <= maxItems - 2) {
            starsAndSubmitButtons[currentSelectable].style.border = cloneInto("1px dashed #e47252", w);
            starsAndSubmitButtons[currentSelectable].style.padding = cloneInto("0px", w);
            submitButton.blur();
        }
        else if (currentSelectable == 6) {
            submitButton.focus();
        }

    }

    addEventListener("keydown", (event) => {

        /*
        keycodes:

        8: Backspace
        9: TAB
        13: Enter
        16: Shift
        27: Escape
        32: Space
        68: D
        107: NUMPAD +
        109: NUMPAD -
        111: NUMPAD /

        49 - 53:  Keys 1-5
        97 - 101: NUMPAD 1-5

         */

        if (event.keyCode >= 49 && event.keyCode <= 53)
            numkey = event.keyCode - 48;
        else if (event.keyCode >= 97 && event.keyCode <= 101)
            numkey = event.keyCode - 96;
        else
            numkey = null;

        // do not do anything if a text area or a input with type text has focus
        if (w.document.querySelector("input[type=text]:focus") || w.document.querySelector("textarea:focus")) {
            return;
        }
        // "analyze next" button
        else if ((event.keyCode === 13 || event.keyCode === 32) && w.document.querySelector("a.button[href=\"/review\"]")) {
            w.document.location.href = "/review";
            event.preventDefault();
        } // submit low quality rating
        else if ((event.keyCode === 13 || event.keyCode === 32) && w.document.querySelector("[ng-click=\"$ctrl.confirmLowQuality()\"]")) {
            w.document.querySelector("[ng-click=\"$ctrl.confirmLowQuality()\"]").click();
            currentSelectable = 0;
            event.preventDefault();

        } // click first/selected duplicate (key D)
        else if ((event.keyCode === 68) && w.document.querySelector("#content > button")) {
            w.document.querySelector("#content > button").click();
            currentSelectable = 0;
            event.preventDefault();

        } // submit duplicate
        else if ((event.keyCode === 13 || event.keyCode === 32) && w.document.querySelector("[ng-click=\"answerCtrl2.confirmDuplicate()\"]")) {
            w.document.querySelector("[ng-click=\"$ctrl.ok()\"]").click();
            currentSelectable = 0;
            event.preventDefault();

        } // submit normal rating
        else if ((event.keyCode === 13 || event.keyCode === 32) && currentSelectable === maxItems) {
            w.document.querySelector("[ng-click=\"reviewCtrl.submitForm()\"]").click();
            event.preventDefault();

        } // close duplicate dialog
        else if ((event.keyCode === 27 || event.keyCode === 111) && w.document.querySelector("[ng-click=\"$ctrl.cancel()\"]")) {
            w.document.querySelector("[ng-click=\"$ctrl.cancel()\"]").click();
            currentSelectable = 0;
            event.preventDefault();

        } // close low quality ration dialog
        else if ((event.keyCode === 27 || event.keyCode === 111) && w.document.querySelector("[ng-click=\"$ctrl.resetLowQuality()\"]")) {
            w.document.querySelector("[ng-click=\"$ctrl.resetLowQuality()\"]").click();
            currentSelectable = 0;
            event.preventDefault();
        }
        // return to first selection (should this be a portal)
        else if (event.keyCode === 27 || event.keyCode === 111) {
            currentSelectable = 0;
        }
        // skip portal if possible
        else if (event.keyCode === 106 || event.keyCode === 220) {
            if (newPortalData.canSkip)
                ansController.skipToNext();
        }
        else if (event.keyCode === 72) {
            showHelp(); // @todo
        }
        // select next rating
        else if ((event.keyCode === 107 || event.keyCode === 9) && currentSelectable < maxItems) {
            currentSelectable++;
            event.preventDefault();
        }
        // select previous rating
        else if ((event.keyCode === 109 || event.keyCode === 16 || event.keyCode === 8) && currentSelectable > 0) {
            currentSelectable--;
            event.preventDefault();
        }
        else if (numkey === null || currentSelectable > maxItems - 2) {
            return;
        }
        // rating 1-5
        else {
            starsAndSubmitButtons[currentSelectable].querySelectorAll("button.five-star-rating__button")[numkey - 1].click();
            currentSelectable++;
        }
        highlight();
    });

    highlight();


}

function getTimer(){
    var t = Date.parse(expireTime) - Date.parse(new Date());

    var isExpired = false;

    if (t > 0) {
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        var timerStr = '';
        if (days) {
            timerStr += days + ' days ';
        }

        if (hours < 10) {
            timerStr += '0';
        }
        timerStr += hours + ':';

        if (minutes < 10) {
            timerStr += '0';
        }

        timerStr += minutes + ':';

        if (seconds < 10) {
            timerStr += '0';
        }

        timerStr += seconds;
    } else {
        timerStr = "Expired"
        isExpired = true;
    }

    return {'timerStr': timerStr, isExpired: isExpired};
}

(function() {
    'use strict';
    shim_GM_notification();
    window.setTimeout(check, 1000);
    GM_addStyle('.sndBtn {position: fixed; top: 3px; left: 3px; z-index:100000;}');
    GM_addStyle('@media screen and (max-width: 1024px) {.sndBtn {left: 52px;}}');
    GM_addStyle('.compatBtn {position: fixed; top: 3px; left: 56px; z-index:100000;}');
    GM_addStyle('#helpIcon {position: fixed; top: 0px; left: 235px; z-index:100000;font-size: x-large;padding: 12px; }');
    GM_addStyle('@media screen and (max-width: 700px) {#helpIcon {display: none;}}');
    GM_addStyle('#timer {position: fixed; top: 3px; left: 55px; z-index:100000; background-color:#F0F0F0;border: 1px solid buttonface;  padding: 12px; margin-top: 0px; height: 45px;}');
    GM_addStyle('@media screen and (max-width: 1024px) {#timer {left: 105px;}}');
    GM_addStyle('.expired {color:#FF0000;}');
    GM_addStyle('#intelLinkEdit {width: fit-content;}');
    GM_addStyle('#mapBtns {float: left; margin-top: 5px;}');
    GM_addStyle('@media screen and (max-width: 410px) {#mapBtns {clear: both; float: right;}}');
    GM_addStyle('#mapBtns .button {margin:2px; padding:5px;}');
    GM_addStyle('#exifDiv {margin-bottom: 5px;}');
    GM_addStyle('#exifDiv .label {margin:1px;}');
    GM_addStyle('#mapBtns img {height: 24px}');
    GM_addStyle('@media screen and (max-width: 410px) {#mapBtns img {height: 28px; margin: 5px;}}');
    GM_addStyle('.transBtn {position: absolute; top: 5px; right: 10px; padding: 5px;}');
    GM_addStyle('.transBtn img {margin:0px; padding:0px; height: 25px; width: 22px; padding-bottom:3px}');
    GM_addStyle('.title-description {position: relative;}');

    GM_addStyle('.tooltip {position:fixed}');
    GM_addStyle('.tooltip-inner {background-color: #252525; max-width:300px;}');

    // GM_addStyle('.modal-body-accordion {overflow: auto; max-height: -webkit-fill-available;}');
    // GM_addStyle('.modal-custom1 {overflow: auto; height: auto;}');
    // GM_addStyle('.reason-help {height: fit-content;}');
    // GM_addStyle('.sbmtAndNxt {margin: 0px 10px 0px 0px;}');
    // GM_addStyle('.answer-btn-container .skip-btn-container {margin: 0px 10px 0px 0px;}');
    // GM_addStyle('.button-primary, .button-secondary {min-width: auto;}');
    // GM_addStyle('.button-primary {padding: 7px 20px!important;}');
    // GM_addStyle('.five-stars {padding: 1px;}');
    // GM_addStyle('@media screen and (min-width: 1025px) {#what-is-it-card-review {height: unset;}}');

    if (userLang.search('-') != -1) {
        userLang = userLang.split('-');
        userLang = userLang[0];
    }

    console.log("init");

    initCtrls();
    init();

})();

function ControlBtns(controlDiv, map, mapType) {
    var gridCtrl = $('<button type="button" class="btn" id="gridCtrl" title="Switch S2 grid layer"><span class="glyphicon glyphicon glyphicon-th"></span></button>');
    $(controlDiv).append(gridCtrl);

    gridCtrl.css("margin-top", "5px");
    gridCtrl.css("margin-left", "5px");

    if (GM_getValue('s2_'+mapType)) {
        gridCtrl.addClass('btn-success');
    } else {
        gridCtrl.addClass('btn-danger');
    }

    gridCtrl.on('click', function (e) {
        gridCtrl.removeClass('btn-success');
        gridCtrl.removeClass('btn-danger');
        if (GM_getValue('s2_'+mapType)) {
            gridCtrl.addClass('btn-danger');
            GM_setValue('s2_'+mapType, 0);
            removeGridForMap(map, mapType);
        } else {
            gridCtrl.addClass('btn-success');
            GM_setValue('s2_'+mapType, 1);
            drawGridForMap(map, mapType);
        }
    });
}


const EXIF_MAP_MARKER = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAA5CAYAAACLWl2QAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAMA0lEQVRoge2aW4xV13nHf2vt27nOGYMpDGDAnmB7DMaAjXyJWztxqWQlllr3IcSumqqtKvUhyoNV9T19qpK+tLKayJGaNq6DHcm1rcROGxRjDx4cqA2GmmEGBpjDXJgLM+ecOZd9XasP+xzmDB3Ac+ZCW81fWpoz+7r2b3/ft9b37QWrWtWqVrWqVa1qVav6fyWxzNc2ABtor7ckoIAqMA2UgACIlrEfLWm5wHQAX3Jgzxpp7MgIsSkhRVYibAE60Np1tS6VtMoXlDoVwGfARWBymfqzYC01mBywLw0vdtnOww9YzoZO08rdZVrWGmkIWwgUmprWXFWRzoehey7wi72BNzQQ+B/X4CDwKVBb4n4tWEsFxgC22/Anm03rD/403bZtj5Ow7pAG+TAUnwcuQ2HIjIrQQEpINpgmXZbDVtPS0yrSxz0v+OfydN+kUj8L4VUgT+x2t0VLASblwFMdhvlXTyfSj30znU0YQohjbpXDtTJXg4CcVrQDCUACLnFwmRKClGHx5WSaxxIpIrT6aXmm2uNWD4/p6PshfAx4S9DHBWuxYDI2PPOAZf/176eyjz6VTMkLQcBbM9NM+x53AZuJI+58N/KAMeLgYpkWX8u2c49l0+1Ww19Uy4f7w+BvQzhCzHJFZSziXNOEJzpN66UD6dxjTyZS1kdelXfLBbKBzwPAeuIh6Ub0TaAN2ACgFJ8EPp4QPJFIy4whN+TDYE1Bq3PAFUAvoq8L1mLA3L3JMF56Pt32u19JphLdbkX0lEtsDEO2EbvNFzFHQQwoBzhacS4MqAh4LJEyBWwcDgNZ1voEMLOIvi5YLYPJwp//djL9Z99Mt2V6A0/8qlxkcxSyqX7RhfqoAFKAozW9YUDStMTjiaQzFIXbroTBhQBOttrXViRbPC+3Rhp/sT+RbtNo8UG1TC4M6KA1KA0JYC2wSSmOV8t4WounEql1bYbxl8ReuWJqFczzO2ync5+TFN1ujRnfpVMIbCmRUiJk/bJCxK1JQsrZ7c37hEBKiSklW4TACH163CoPOwnxJdN+GHimxb62JLOFczLAc19LZcSMjjjjVdmUzbJ161asVAoAFQTMDA+DlKTWrmVmeJja9DTZjg7S69dTGhrCsG2Sa9ZQzOeRpkluyxYMxwEgrNUo5vNcLFeopjL8TiJpdHu154FDxCnEsqsVMPdYsGWfneRM4FEOAh7Z9SBPfuc7JHI5qlNTeIUCZ995B5Ri54EDXD56lMHubnZ/61uk7ryTT155hQ0PPcS2p5/m+Msv47S38+i3v40KQ9xikcqVK9QOHuTcqVNc8H0ed1LA1H3EqUZ+aRHMr1bA3NUhjdxa0+CKG6KVor2tjXVdXUwNDHDh0CG8QoHpgQFUFBH6PrteeIFN+/bRuX8/R773Pcqjo2SffZbf2rkTJ5cjtXYtHbt30/fzn3Px/ffxZ2bwxsYwlWIsCthvpklDeyWeFv2vBbN2s2WltYZCFCG0igOulKzr6gJgZniYQj7P+KlTnHnjDb7y3e+y88ABBo8c4fPXX7/hhTv27iXR3s7U+fNM9PeTAKajEIB10kpXVLC2hf62pFaCbyIjpKkBXysE9VFIa9zpaYqDgxQvXyaoVNBKocIQaRiYySTSMAjdG09iqxMTFC5dYmZkhMjzsABPawI0WUOYgNPKQ7aiViwm9HQ8CzUQaOIpqVaK8TNn6H3zTbxyGbdQINPRwY5vfAOlNadfe427v/pVdr34Yhx/5tHZt9/m5I9/DFoTRhER4CAwEPgKBYQtPueC1QqY6SsqqAoBaUOihMB3XSoTE2zat4+127fjl8ucPniQoFymfds2Tr/6Kufeew+3UODe555jsq8Pt1hkZmSE0HXxKxWKQ0O4hQIqiAediDhBWi8lEhhVYaO4tSJqZS6214Ef9XRs3fMbr8ZbxSkeuaOdzq4u7EwGAB2GFC5fxjBN7EyGybNncQsFUuvWseGhhygMDoLWpNev52pfH9KyuPP++7na3x8P88QFmc+E4Mm2NTziJHl2fOgI8MfEOeeyqxWLuejB4DHf3bPFtHAMg9HJSazu7lueWJ2Y4MKhQ9f+nzp//trv8ujonGMrgDRMtpg2PV4NoB8YaaG/LamV4FsAfv3rWkXnpMFm22ECQcDSpb8amADaLZu0FBz1agr4BStYm2kFjAZeP+G5I/2BzxNOCs80GWFpwGjgKnBVGjycSNEf+Jz1/c+Af1+Cy39htZorjY9H6pVDtUpto2mxJ5XhopAU4Noo1Yo0sQudA+5OpthiWhx13VJRhT+o72pFkji3NYhDh1HfdtP42nLZwUOP+1rvvMs0tz6eSIlLSolLYUAbcS2GW935OmnigNsH2E6SP8zk9EDgR7+slQ+NafWP3HpEkvVmEc93EsTFw2T9d6Jpe+OvXT++YSDX3uliClWVmlIGQux+1Em27XAcMaAUg2FIEj3nbjcDpImH5jLQLwSmk+JAtp20lPqNyszg6cD7YQhHmX8O07AGh7ic04DQeOCGhdyomfXj7HozG91dDJgogOmSUpvbDeP+pxIpq8tOMC6gNwqp6LjA3+jZ9XA0cZpcBIaAvDTYkEzzR213sN2yebtaqbxbK79V0vonxGHneiB2HUQDiMVcN2luN1LzMQ1QDmAuBgxApay1H2m1a4tpdeyyE+ywExiGwSSCS0pxRWvKxG7iEgeKAnER9yIwKiQpJ8Ej6SwvZHNss2yOea7+WWXm1LkweBk4zdzPKBazLuLwBeLFAnQN0mLBRMB4NYruQIjdXbbjbDRN0Wna3O8k2GonsE2LCSHIE4PICygYJqZlc28yzTOZHL+XzvJEIsmd0iAfBvon5dL0cbf6Ty68yezHt0b1M0UMpGGIy/E1VbQywbtexSk4+IFbfXC7aX/9hUzOyUpJp7TZZtl8OZnG1YpAayIdJ1mGEFhCkBACR8hrjl1SinerZe9Dr/of0/AG8ecn6ruzxEBE07Zl02ItpqFiSeviWBTu3G7Z6++xbCGEEIYQ2EKQkpKsNMhKSZs0aDMM0lLiCIkhBKJe4vygVgl/UJ7+NB9FfwecIA5FEkgz+3lquaxkjpYKjAImp5TKlJTa9UwylY7RXFfvbYLQLK0101HE35Sujp/w/R8B7xCHJMFsPFkRIA0tFRgAV8NQPgw6Owxrx322I6x5IMynqlb8tFzSr1VK/6bgH4gzAogDbYrZnO7/JBiAgoKxySj6+r2Wnd5omshbwAm15mOvxg9LhdFRFb0E9NZ3CWYnYytqLbD0YABGfXSHAXt3WI6RkXJe94HYhUbDkFfLRe8/A/cVV+t/ZXZobsxTFvOpqmUtBxgVaj3hafXgRtPafLdlC/MGYFyt+VWtot6uzhwZiqLvA821h9tmLbA8YFBQKCslI9j7oO1k10jjfwRirTXnQ1//S7k4dNL3/j6Ew8xO+03i2HJbrAVaz65vJbcKh0/6bs9Rt+YH8+TbntYc8Wrep4HX7cKHzF3q0Zje3xYogF4uMBoYHImi9z7yqmOXggCtZ+EorbkQ+rqnVh0Zi6J3idOlhgzmZrwrpUbFRAHBct68FsGHp33v2DG3FlW1QmuN1pqKVvS4tejzIPiIeGFQc2WuAWYl1YASEltuebnfSn48Ct//je9OjYXRtR6MhCHHPXd8MgoOA8NNxwvi0WgpE8ObqWEhDSCVeltWiwEIynC0z/d7+wJPhfUe9Ppe1B/4/1WN19g1r/FtpP0rBcUnXnNcJgbiUZ8urIQfXzgfeh+c8N1CRSk9o0L9iV+bGgj994HL1x27VPMWzdyYoYhfQECcrZeAKeJyUIUY0JwVoisBZqYKR4553vBQGOjBINTHPTfvQw/xm2qoUZJsBcp8IELiB264SIm4FFQihhNyk/L0UpQdbiUFDPQGbt9AENznaqX7grAXuNTUMcmNF3feSI1zFXOtotGaty+4Pr8SYABGi1qf7Paq+6taRTXUSeKVrDCbE91sKZlu+tsoEzcDCFkEhPm0UmCqwKkPa5WrLjoATjE7RNvMro1u1nyu0QDQgLJsS1xXCgzAmWEVXVHxA51tun/yun40QATEIK53jRXRSoK5HMUz3IA4WZTE+VCzCzWCZTOM26L/Bv1YRwFVWm54AAAAAElFTkSuQmCC`;
